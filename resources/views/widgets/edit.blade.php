<?php  $page = 'widgets'; ?>
@extends('layouts.main')

@section('content')



    <section class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/widgets"><i class="fas fa-cogs"></i> Widgets</a></li>
                        <li class="breadcrumb-item active"><a href="/widgets/{{ $widget->id }}">{{ $widget->name }}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">edit</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <form  method="POST" action="/widgets/{{ $widget->id }}">
        <input name="_method" type="hidden" value="PUT">
        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
        <section id="title-section">
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <h1>Update: {{ $widget->name }}</h1>
                    </div>
                    <div class="col-6 small-options">
                        <button id="add-to-backet"  class="red-button" >Save</button>
                    </div>
                </div>
            </div>
        </section>



        <section>
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <div class="create-table">

                            <table class="table">

                                <tbody>
                                <tr>
                                    <td>Name:</td>
                                    <td><input type="text" value="{{ $widget->name }}" name="name" class="form-control"/></td>
                                </tr>
                                <tr>
                                    <td>Description:</td>
                                    <td><textarea name="description"  class="form-control">{{ $widget->description }}</textarea></td>
                                </tr>
                                </tbody>

                            </table>

                        </div>

                    </div>
                    <div class="col-6">
                        <div class="create-table">

                            <table class="table">


                                <tbody>
                                <tr>
                                    <td>
                                        <h4>Select Pack Sizes:</h4><br/>
                                        <?php
                                            $current_packs = [];
                                            foreach($widget->packs as $pack)
                                            {
                                                $current_packs[] = $pack['id'];
                                            }
                                            foreach($packs as $pack)
                                            {
                                                if($pack->id){
                                                ?>
                                                <input type="checkbox" name="packs[]" value="{{ $pack->id }}" <?php if (in_array($pack->id, $current_packs)) {echo "checked";}?>>
                                                <label for="vehicle1"> {{ $pack->size }}</label><br>
                                                <?php
                                            }
                                        }


                                        ?>



                                        <br/>
                                        <h4>Need more sizes?</h4>
                                        <p>Add some more pack sizes first</p>
                                        <a href="/packs/create" style="color:red;font-eight:800;text-decoration: underline;"/>Create Pack New Size
                                    </td>
                                </tr>
                                </tbody>

                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </section>
    </form>

    <!-- Modal -->
    <div class="modal fade" id="add-to-basket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add to basket</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4>Widget Name</h4>
                    <p>Please select the quantity you would like to purchase:</p>
                </div>
                <div class="modal-footer">
                    <form class="add-to-basket-form" action="test.php" method="POST">
                        <span>Quantity:</span>
                        <input type="text" name="quantity" class="form-control" width="50px"/>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>
                        <button class="btn btn-primary">Add to basket</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <br/><br/>

@endsection()
