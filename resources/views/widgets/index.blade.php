<?php  $page = 'widgets'; ?>
@extends('layouts.main')

@section('content')


    <section class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Widgets</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section id="title-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6">
                <h1>List of Widgets</h1>
            </div>
            <div class="col-12 small-options col-sm-6">
                <a href="/widgets/create" class="red-button" style="color: white;font-weight: 400;padding: 8px 15px;font-size: 18px;">Create Widget</a>
            </div>
        </div>
    </div>
</section>

<section class="video-section">
    <div class="container">
        <div class="row">
            <?php $count=1; ?>
            @foreach($widgets as $widget)
                <div class="col-12 col-sm-6 col-lg-6 col-xl-3" style="max-width:330px;">
                    <div class="widget-box">
                        <a href="/widgets/{{ $widget->id }}" class="no-link-style">
                            @if($count > 4)
                                <img src="images/img-placeholder.jpg" alt="image" class="img-fluid"/>
                            @else
                                <img src="images/img-{{ $count }}.jpg" alt="image" class="img-fluid"/>
                            @endif
                            <div class="text-box">
                                <h2>{{ $widget->name }}</h2>
                                <i class="fas fa-long-arrow-alt-right"></i>
                            </div>
                        </a>
                    </div>
                </div>
                    <?php $count++; ?>
            @endforeach
        </div>
    </div>
</section>


@endsection
