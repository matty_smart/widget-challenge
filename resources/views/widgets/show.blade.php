<?php  $page = 'widgets'; ?>
@extends('layouts.main')

@section('content')


<section class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/widgets"><i class="fas fa-cogs"></i> Widgets</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ $widget->name }}</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section id="title-section">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>{{ $widget->name }}</h1>
            </div>
            <div class="col-6 small-options">
                <span>Product ID: {{ $widget->id }} </span>
                <button id="add-to-backet"  class="red-button" data-toggle="modal" data-target="#add-to-basket">Add to basket</button>
                <a href="/widgets/{{ $widget->id }}/edit"  class="red-button" style="background-color: gray;color: white;font-weight: 500;padding: 7px 15px;" >Edit Widget</a>
                <form method="POST" action="/widgets/{{ $widget->id }}" style="display:inline-block;">
                    <input name="_method" type="hidden" value="DELETE">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                    <button type="submit" style="border: 0; background-color: gray; color: white; padding: 5px 15px; border-radius: 8px; font-size: 18px;">Delete</button>
                </form>
            </div>
        </div>
    </div>
</section>


<!-- Modal -->
<div class="modal fade" id="add-to-basket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add to basket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4>Widget Name</h4>
                <p>Please select the quantity you would like to purchase:</p>
            </div>
            <div class="modal-footer">
                <form class="add-to-basket-form" action="/go-to-checkout" method="POST">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                    <input name="widget_id" type="hidden" value="{{ $widget->id }}"/>
                    <span>Quantity:</span>
                    <input type="text" name="quantity" class="form-control" width="50px"/>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>
                    <button class="btn btn-primary red">Checkout</button>
                </form>
            </div>
        </div>
    </div>
</div>


<section class="video-section">
    <div class="container">


        <div class="row margin-top-24">
            <div class="col">

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-file-alt"></i> Description</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <h3>{{ $widget->name }}</h3>
                        <p>{{ $widget->description }}</p>

                        <h5>Available Pack sizes:</h5>
                        <p style="color:#d14747;font-weight:600;">
                            <?php


                            ?>

                            @foreach($packs as $pack)
                                {{ $pack }},
                            @endforeach()
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <br/><br/>

@endsection()
