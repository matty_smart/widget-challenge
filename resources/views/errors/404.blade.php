<?php  $page = 'home'; ?>
@extends('layouts.main')

@section('content')

    <style>
        body{background-color:#292929 !important;}
    </style>

    <section class="video-section margin-top-24">
        <div class="container">
            <div class="row">
                <div class="col-6 offset-3">
                    <div class="jumbotron ">
                        <h1 class="display-4">404 - Page not found</h1>
                        <br/><br/>
                        <h5>Jump back into the Task:</h5><br/>
                        <p class="lead">
                            <a class="btn btn-primary btn-lg" href="/widgets" role="button" style="background-color:#d14747;border:0;margin-right:10px;">START THE CHALLENGE</a>
                            <a class="btn btn-primary btn-lg" href="/code" role="button" style="background-color:gray;border:0;">VIEW THE CODE</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
