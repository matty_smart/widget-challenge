<?php $page = 'code'; ?>
@extends('layouts.main')

@section('content')

    <section id="title-section">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Widget Challenge Calculations</h1>
            </div>
        </div>
    </div>
</section>

<section class="video-section">
    <div class="container">
        <div class="row margin-top-24">
            <div class="col">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#checkout" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-code"></i>CheckoutController + Class</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="fas fa-code"></i>WidgetsController + Eloquent Model</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"><i class="fas fa-code"></i>Packs Eloquent Model</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent" style="padding-left:0;padding-right:0;">
                    <div class="tab-pane fade show active" id="checkout" role="tabpanel" aria-labelledby="home-tab">
                        <p style="padding-left:24px;font-weight:500;">Developed with Laravel 7</p>
                        <pre>
                            <code class="language-php">
class CheckoutController extends Controller
{
    public function index(Request $request)
    {
        $this->validate($request, [
            'quantity' => 'integer|min:1|',
        ]);

        $widget = Widget::find($request->widget_id);
        $packs = Pack::getPacksByWidgetId($request->widget_id);

        $checkout = new Checkout;
        $results = $checkout->checkout($request->widget_id,$request->quantity);

        return view('checkout.index',['results'=>$results,'widget'=>$widget,'quantity'=>$request->quantity,'packs'=>$packs]);
    }
}

class Checkout
{
    private $packsToSend;
    private $total;
    private $qtyLeft;

    public function checkout($widget_id, $qty): array
    {
        $this->qtyLeft = $qty;
        $packs = Pack::getPacksByWidgetId($widget_id);

        return $this->performCalculation($packs,$qty);
    }

    public function performCalculation($packs, $qty): array
    {
        //Loop through packs in reverse order (Highest to Lowest)
        for($i=count($packs)-1;$i>=0;$i--)
        {
            if($this->qtyLeft - $this->total <= $packs[$i]){
                if($this->qtyLeft - $packs[$i] >= 0 ){
                    self::calculateIfMultipleNeededAndAddPacks($packs,$i,$qty);
                }else{
                    if($this->qtyLeft - $packs[$i] < $packs[0] && $this->qtyLeft - $packs[$i] > $packs[0] - ($packs[0] * 2)){
                        self::addPack($packs,$i);
                    }
                }
            }else{
                self::calculateIfMultipleNeededAndAddPacks($packs,$i,$qty);
            }
        }
        return ['total'=>$this->total,'packsToSend'=>$this->packsToSend];
    }

    public function addPack($packs, $i)
    {
        $this->total += $packs[$i];
        $this->qtyLeft = $this->qtyLeft - $packs[$i];
        $this->packsToSend[$i] = ['pack_size'=>$packs[$i],'pack_num'=>1];
    }

    public function calculateIfMultipleNeededAndAddPacks($packs, $i, $qty)
    {
        $this->total += $packs[$i];
        $tempCounter = 1;
        while($this->total < $qty)
        {
            if($this->total + $packs[$i] <= $qty || $this->total + $packs[$i] - $qty < $packs[0]){
                $this->total += $packs[$i];
                $tempCounter++;
            }else{
                break;
            }
        }
        $this->qtyLeft = $this->qtyLeft - ($packs[$i] * $tempCounter);
        $this->packsToSend[$i] = ['pack_size'=>$packs[$i],'pack_num'=>$tempCounter];
    }
}
                            </code>
                        </pre>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <p style="padding-left:24px;font-weight:500;">Developed with Laravel 7</p>
                        <pre>
                        <code class="language-php">
class WidgetController extends Controller
{
    public function index()
    {
        $widgets = Widget::all();
        return view('widgets.index',['widgets'=>$widgets]);
    }

    public function create()
    {
        $packs = Pack::orderBy('size', 'ASC')->get();
        return view('widgets.create',['packs'=>$packs]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:4|', 'description' => 'required|min:10|','packs'=>'required'
        ]);

        //Create new Widget
        $widget = new Widget;
        $widget->name = $request->name;
        $widget->description = $request->description;
        $widget->save();

        //attach packs to widget
        foreach($request->packs as $pack_id){

            $values = array('pack_id' => $pack_id,'widget_id' => $widget->id );
            DB::table('pack_widget')->insert($values);

        }
        return redirect('/widgets/'.$widget->id)->with('status','Widget was successfully created');
    }

    public function show($id)
    {
        $widget = Widget::find($id);

        $packs = [];
        foreach($widget->packs as $pack){
            $packs[] = $pack->size;
        }

        sort($packs);

        return view('widgets.show',['widget'=>$widget,'packs'=>$packs]);
    }

    public function edit($id)
    {
        $widget = Widget::find($id);
        $packs = Pack::orderBy('size', 'ASC')->get();
        return view('widgets.edit',['widget'=>$widget,'packs'=>$packs]);
    }

    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'name' => 'required|min:4|', 'description' => 'required|min:10|','packs'=>'required'
        ]);

        $widget = Widget::find($id);
        $widget->name = $request->name;
        $widget->description = $request->description;
        $widget->update();

        DB::delete('delete from pack_widget where widget_id = ?',[$widget->id]);

        foreach($request->packs as $pack){
            $values = array('pack_id' => $pack,'widget_id' => $widget->id );
            DB::table('pack_widget')->insert($values);
        }

        return redirect('/widgets/'.$request['id'])->with('status','Widget successfully updated!');
    }

    public function destroy($id)
    {
        $widget = Widget::find($id);
        $widget->delete();
        return redirect('/widgets/')->with('status','Widget was deleted!');
    }

}

class Widget extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'widgets';

    protected $fillable = [
        'id','name','description'
    ];

    /**
     * The packs that belong to the widget.
     */
    public function packs()
    {
        return $this->belongsToMany(Pack::class);
    }
}
                        </code>
                        </pre>
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <p style="padding-left:24px;font-weight:500;">Developed with Laravel 7</p>
                        <pre>
                        <code class="language-php">
class Pack extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'packs';

    protected $fillable = [
        'id','size'
    ];

    /**
     * The widgets that belong to the pack.
     */
    public function widgets()
    {
        return $this->belongsToMany(Widget::class);
    }

    public static function getPacksByWidgetId($widget_id)
    {
        $widget = Widget::find($widget_id);
        $packs = [];

        foreach($widget->packs as $pack){
            $packs[] = $pack['size'];
        }
        sort($packs);

        return $packs;
    }
}
                        </code>
                        </pre>
                    </div>
                </div>



            </div>
        </div>
    </div>
</section>
