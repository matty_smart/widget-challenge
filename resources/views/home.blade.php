<?php  $page = 'home'; ?>
@extends('layouts.main')

@section('content')

    <style>
        body{background-color:#292929 !important;}
    </style>

    <section style="margin-top:100px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-12">
                    <h1 style="margin-top: 80px;color:white;margin-right:10px;">Wally's Widget Challenge</h1> <span style="color:white;display: block;margin-bottom: 60px;"> by Matthew Smart</span>
                </div>
                <div class="col-lg-6 col-12">
                    <p class="lead" style="margin-top:80px;text-align: right;">
                        <a class="btn btn-primary btn-lg" href="/widgets" role="button" style="font-size:17px;background-color:#d14747;border:0;margin-right:10px;">START THE CHALLENGE</a>
                        <a class="btn btn-primary btn-lg" href="/code" role="button" style="font-size:17px;background-color:gray;border:0;">VIEW THE CODE</a>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="video-section margin-top-24" style="margin-top:30px;">
        <div class="container">
            <div class="row">
                <div class="col">

                    <div class="jumbotron" style="margin-top:0;">
                        <h3>About the challenge:</h3><br/>

                        <p>Wally’s Widget Company is a widget wholesaler. They sell widgets in a variety of pack sizes:</p>
                        <ul>
                            <li>250 widgets</li>
                            <li>500 widgets</li>
                            <li>1,000 widgets</li>
                            <li>2,000 widgets</li>
                            <li>5,000 widgets</li>
                        </ul>
                        <p>
                            Their customers can order any number of widgets, but they will always be given complete packs.
                            The company wants to be able to fulfil all orders according to the following rules:
                        </p>
                        <ul>
                            <li>1. Only whole packs can be sent. Packs cannot be broken open.</li>
                            <li>2. Within the constraints of Rule 1 above, send out no more widgets than necessary to fulfil</li>
                            <li>3. Within the constraints of Rules 1 & 2 above, send out as few packs as possible to fulfil each order.</li>
                        </ul>

                        <br/>
                        <h5>For example:</h5>
                        <br/>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Widgets ordered</th>
                                    <th>Correct packs to send</th>
                                    <th>Incorrect solution(s)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>250 x 1</td>
                                    <td>500 x 1 (too many widgets)</td>
                                </tr>
                                <tr>
                                    <td>250</td>
                                    <td>250 x 1</td>
                                    <td>500 x 1 (too many widgets)</td>
                                </tr>
                                <tr>
                                    <td>251</td>
                                    <td>500 x 1</td>
                                    <td>250 x 2 (too many packs)
                                        <br/><strong>Pay attention to this one – many
                                            people get this wrong!</strong></td>
                                </tr>
                                <tr>
                                    <td>501</td>
                                    <td>500 x 1<br/>
                                        250 x 1</td>
                                    <td>1,000 x 1 (too many widgets)<br/>
                                        250 x 3 (too many packs)</td>
                                </tr>
                                <tr>
                                    <td>12,001</td>
                                    <td>5,000 x 2<br/>
                                        2,000 x 1<br/>
                                        250 x 1</td>
                                    <td>5,000 x 3 (too many widgets)</td>
                                </tr>
                            </tbody>
                        </table>
                        <br/>
                        <h5>Your task</h5>
                        <br/>
                        <p>Write a program that will tell Wally’s Widgets what packs to send out, for any given order size.</p>
                    </div>
                        <p class="lead" style="margin-top:30px;margin-bottom:60px;">
                            <a class="btn btn-primary btn-lg" href="/widgets" role="button" style="font-size:17px;background-color:#d14747;border:0;margin-right:10px;">START THE CHALLENGE</a>
                            <a class="btn btn-primary btn-lg" href="/code" role="button" style="font-size:17px;background-color:gray;border:0;">VIEW THE CODE</a>
                        </p>
                </div>
            </div>
        </div>
    </section>


@endsection
