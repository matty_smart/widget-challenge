<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
Route::get('/code', 'HomeController@code');
Route::post('/go-to-checkout/', 'CheckoutController@index');
Route::get('/go-to-checkout/', 'CheckoutController@get');


Route::get('/widgets/', 'WidgetController@index');
Route::get('/widgets/create/', 'WidgetController@create');
Route::post('/widgets/', 'WidgetController@store');
Route::get('/widgets/{id}', 'WidgetController@show');
Route::get('/widgets/{id}/edit', 'WidgetController@edit');
Route::put('/widgets/{id}', 'WidgetController@update');
Route::delete('/widgets/{id}', 'WidgetController@destroy');


















Route::get('/basket', 'HomeController@basket');

