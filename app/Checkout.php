<?php

namespace App;

class Checkout
{
    private $packsToSend;
    private $total;
    private $qtyLeft;

    public function checkout($widget_id, $qty): array
    {
        $this->qtyLeft = $qty;
        $packs = Pack::getPacksByWidgetId($widget_id);

        return $this->performCalculation($packs,$qty);
    }

    public function performCalculation($packs, $qty): array
    {
        //Loop through packs in reverse order (Highest to Lowest)
        for($i=count($packs)-1;$i>=0;$i--)
        {
            if($this->qtyLeft - $this->total <= $packs[$i]){
                if($this->qtyLeft - $packs[$i] >= 0 ){
                    self::calculateIfMultipleNeededAndAddPacks($packs,$i,$qty);
                }else{
                    if($this->qtyLeft - $packs[$i] < $packs[0] && $this->qtyLeft - $packs[$i] > $packs[0] - ($packs[0] * 2)){
                        self::addPack($packs,$i);
                    }
                }
            }else{
                self::calculateIfMultipleNeededAndAddPacks($packs,$i,$qty);
            }
        }
        return ['total'=>$this->total,'packsToSend'=>$this->packsToSend];
    }

    public function addPack($packs, $i)
    {
        $this->total += $packs[$i];
        $this->qtyLeft = $this->qtyLeft - $packs[$i];
        $this->packsToSend[$i] = ['pack_size'=>$packs[$i],'pack_num'=>1];
    }

    public function calculateIfMultipleNeededAndAddPacks($packs, $i, $qty)
    {
        $this->total += $packs[$i];
        $tempCounter = 1;
        while($this->total < $qty)
        {
            if($this->total + $packs[$i] <= $qty || $this->total + $packs[$i] - $qty < $packs[0]){
                $this->total += $packs[$i];
                $tempCounter++;
            }else{
                break;
            }
        }
        $this->qtyLeft = $this->qtyLeft - ($packs[$i] * $tempCounter);
        $this->packsToSend[$i] = ['pack_size'=>$packs[$i],'pack_num'=>$tempCounter];
    }
}
