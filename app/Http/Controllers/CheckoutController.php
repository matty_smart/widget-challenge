<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Widget;
use App\Pack;
use App\Checkout;

class CheckoutController extends Controller
{

    public function index(Request $request)
    {
        $this->validate($request, [
            'quantity' => 'integer|min:1|',
        ]);

        $widget = Widget::find($request->widget_id);
        $packs = Pack::getPacksByWidgetId($request->widget_id);

        $checkout = new Checkout;
        $results = $checkout->checkout($request->widget_id,$request->quantity);

        return view('checkout.index',['results'=>$results,'widget'=>$widget,'quantity'=>$request->quantity,'packs'=>$packs]);
    }

    public function get()
    {
        return view('errors.405');
    }
}
