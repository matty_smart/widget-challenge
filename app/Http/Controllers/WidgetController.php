<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Widget;
use App\Pack;
use Illuminate\Support\Facades\DB;

class WidgetController extends Controller
{
    public function index()
    {
        $widgets = Widget::all();
        return view('widgets.index',['widgets'=>$widgets]);
    }

    public function create()
    {
        $packs = Pack::orderBy('size', 'ASC')->get();
        return view('widgets.create',['packs'=>$packs]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:4|', 'description' => 'required|min:10|','packs'=>'required'
        ]);

        //Create new Widget
        $widget = new Widget;
        $widget->name = $request->name;
        $widget->description = $request->description;
        $widget->save();

        //attach packs to widget
        foreach($request->packs as $pack_id){

            $values = array('pack_id' => $pack_id,'widget_id' => $widget->id );
            DB::table('pack_widget')->insert($values);

        }
        return redirect('/widgets/'.$widget->id)->with('status','Widget was successfully created');
    }

    public function show($id)
    {
        $widget = Widget::find($id);

        $packs = [];
        foreach($widget->packs as $pack){
            $packs[] = $pack->size;
        }

        sort($packs);

        return view('widgets.show',['widget'=>$widget,'packs'=>$packs]);
    }

    public function edit($id)
    {
        $widget = Widget::find($id);
        $packs = Pack::orderBy('size', 'ASC')->get();
        return view('widgets.edit',['widget'=>$widget,'packs'=>$packs]);
    }

    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'name' => 'required|min:4|', 'description' => 'required|min:10|','packs'=>'required'
        ]);

        $widget = Widget::find($id);
        $widget->name = $request->name;
        $widget->description = $request->description;
        $widget->update();

        DB::delete('delete from pack_widget where widget_id = ?',[$widget->id]);

        foreach($request->packs as $pack){
            $values = array('pack_id' => $pack,'widget_id' => $widget->id );
            DB::table('pack_widget')->insert($values);
        }

        return redirect('/widgets/'.$request['id'])->with('status','Widget successfully updated!');
    }

    public function destroy($id)
    {
        $widget = Widget::find($id);
        $widget->delete();
        return redirect('/widgets/')->with('status','Widget was deleted!');
    }

}
