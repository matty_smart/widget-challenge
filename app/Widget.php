<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pack;

class Widget extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'widgets';

    protected $fillable = [
        'id','name','description'
    ];

    /**
     * The packs that belong to the widget.
     */
    public function packs()
    {
        return $this->belongsToMany(Pack::class);
    }
}
