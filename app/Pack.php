<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pack extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'packs';

    protected $fillable = [
        'id','size'
    ];

    /**
     * The widgets that belong to the pack.
     */
    public function widgets()
    {
        return $this->belongsToMany(Widget::class);
    }

    public static function getPacksByWidgetId($widget_id)
    {
        $widget = Widget::find($widget_id);
        $packs = [];

        foreach($widget->packs as $pack){
            $packs[] = $pack['size'];
        }
        sort($packs);

        return $packs;
    }
}
