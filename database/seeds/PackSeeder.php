<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packs')->insert([
            'size' => 250
        ]);
        DB::table('packs')->insert([
            'size' => 500
        ]);
        DB::table('packs')->insert([
            'size' => 1000
        ]);
        DB::table('packs')->insert([
            'size' => 2000
        ]);
        DB::table('packs')->insert([
            'size' => 5000
        ]);
    }
}
