<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PackWidgetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<5;$i++)
        {
            for($u=1;$u<6;$u++)
            {
                DB::table('pack_widget')->insert([
                    'widget_id' => $i,
                    'pack_id' => $u
                ]);
            }
        }
    }
}
