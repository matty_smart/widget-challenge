<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WidgetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('widgets')->insert([
            'name' => 'Widget 1',
            'description' => 'Here is the description for widget 1',
        ]);
        DB::table('widgets')->insert([
            'name' => 'Widget 2',
            'description' => 'Here is the description for widget 2',
        ]);
        DB::table('widgets')->insert([
            'name' => 'Widget 3',
            'description' => 'Here is the description for widget 3',
        ]);
        DB::table('widgets')->insert([
            'name' => 'Widget 4',
            'description' => 'Here is the description for widget 4',
        ]);
    }
}
